class AssetCatalog {
  static String loginContainerBackground = 'assets/background/globe.png';
  static String entieLogo = 'assets/icons/entie_logo.png';
  static String googleLogo = 'assets/icons/google_1.png';
  static String closeIcon = 'assets/icons/close_grey.png';

  static String startUpRocketIcon = 'assets/icons/rocket.png';
  static String cofounderIcon     = 'assets/icons/cofounder.png';
  static String investorIcon      = 'assets/icons/investor.png';
  static String manufactureIcon   = 'assets/icons/manufacturing.png';
  static String filterIcon        = 'assets/icons/filter_icon.png';
  static String verifiedIcon        = 'assets/icons/verified.png';
}