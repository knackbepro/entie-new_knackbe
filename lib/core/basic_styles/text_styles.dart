import 'package:entie_flutter/core/local_assets/common_images.dart';
import 'package:flutter/material.dart';

class NewEntieStyle {
  static Color primaryColor = Color(0xFF14264e);
  static Color primaryYellowColor = Color(0xFFfeba12);
  static Color greyColor = Color(0xFFf2f2f2);
  static Color greyShadeColor = Color(0xFFc7c7c7);
  static Color greyShade1Color = Color(0xFF707070);
  static Color greyShade2Color = Color(0xFF656565);

  static BoxDecoration myBoxDecoration = const BoxDecoration(
    color: Colors.white,
    borderRadius: BorderRadius.all(Radius.circular(4)),
    boxShadow: [
      BoxShadow(
        offset: Offset(0.5, 0.5),
        color: Colors.grey,
        blurRadius: 5,
        spreadRadius: 2,
      ),
    ],
  );

  static BoxDecoration bgBoxDecoration = BoxDecoration(
    image: DecorationImage(
      fit: BoxFit.cover,
      image: AssetImage(AssetCatalog.loginContainerBackground),
    ),
  );

  static TextStyle textStyleXX =
      TextStyle(color: primaryColor, fontSize: 20, fontFamily: 'MuliFamily');

  static TextStyle textStyleXXX =
      TextStyle(color: primaryColor, fontSize: 30, fontFamily: 'MuliFamily');

  static TextStyle textStyleXIII =
      TextStyle(color: primaryColor, fontSize: 13, fontFamily: 'MuliFamily');

  static TextStyle textStyleXII =
      TextStyle(color: primaryColor, fontSize: 12, fontFamily: 'MuliFamily');

  static TextStyle textStyleXI =
      TextStyle(color: primaryColor, fontSize: 11, fontFamily: 'MuliFamily');

  static TextStyle textStyleX =
      TextStyle(color: primaryColor, fontSize: 10, fontFamily: 'MuliFamily');

  static TextStyle textStyleWhiteXIV =
      TextStyle(color: Colors.white, fontSize: 14, fontFamily: 'MuliFamily');

  static TextStyle textStyleWhiteXII =
      TextStyle(color: Colors.white, fontSize: 12, fontFamily: 'MuliFamily');

  static TextStyle textStyleWhiteXI =
      TextStyle(color: Colors.white, fontSize: 11, fontFamily: 'MuliFamily');

  static TextStyle textStyleWhiteX =
      TextStyle(color: Colors.white, fontSize: 10, fontFamily: 'MuliFamily');


  static TextStyle textStyleGreyXII =
  TextStyle(color: greyShade1Color, fontSize: 12, fontFamily: 'MuliFamily');

  static TextStyle textStyleGreyX =
  TextStyle(color: greyShade1Color, fontSize: 10, fontFamily: 'MuliFamily');

  static ButtonStyle primaryButtonStyle = ElevatedButton.styleFrom(
    primary: const Color(0xFF14264e),
    onPrimary: Colors.white,
  );

  static BoxDecoration roleBoxDecoration = BoxDecoration(
    color: Color(0Xfff2f2f2),
    borderRadius: BorderRadius.all(Radius.circular(4)),
    boxShadow: [
      BoxShadow(
        offset: const Offset(0.5, 0.5),
        color: Colors.grey,
        blurRadius: 5,
        spreadRadius: 2,
      ),
    ],
  );
}
