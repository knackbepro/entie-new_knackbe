import 'package:entie_flutter/core/basic_styles/text_styles.dart';
import 'package:entie_flutter/features/registration/presentation/screen/registration_screen.dart';
import 'package:flutter/material.dart';
import 'package:entie_flutter/core/local_assets/common_images.dart';

class LoginPageMain extends StatefulWidget {
  LoginPageMain({Key? key}) : super(key: key);

  @override
  State<LoginPageMain> createState() => _LoginPageMainState();
}

class _LoginPageMainState extends State<LoginPageMain> {
  final TextEditingController? _usernameController = TextEditingController();
  final TextEditingController? _passwordController = TextEditingController();

  bool _isObscure = true;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: Container(
        width: double.infinity,
        decoration: NewEntieStyle.bgBoxDecoration,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Image.asset(
              AssetCatalog.entieLogo,
              width: 90.0,
              height: 71.0,
            ),
            Container(
              width: double.infinity,
              margin: const EdgeInsets.only(
                top: 23.0,
                left: 8,
                right: 8,
              ),
              padding: const EdgeInsets.only(
                top: 14.0,
                bottom: 22.0,
                left: 10,
                right: 10,
              ),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(4.0),
                color: const Color(0XdfFFFFFF).withOpacity(0.8),
                boxShadow: const [
                  BoxShadow(
                      offset: Offset(0.0, 6.0),
                      // color: Colors.grey,
                      color: Colors.transparent,
                      blurRadius: 6,
                      spreadRadius: 0.75),
                ],
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const Text(
                    'Mobile Number',
                    style: TextStyle(
                      color: Color(0xFF14264e),
                      fontSize: 13.0,
                      fontFamily: 'MuliFamily',
                    ),
                  ),
                  // const SizedBox(
                  //   height: 8.0,
                  // ),
                  TextFormField(
                    decoration: const InputDecoration(
                        border: UnderlineInputBorder(),
                        labelText: 'Mobile Number',
                        focusColor: Colors.black54),
                    style: const TextStyle(
                      fontSize: 14.0,
                    ),
                    // controller: _usernameController,
                  ),
                  const SizedBox(
                    height: 12.0,
                  ),
                  const Text(
                    'Password',
                    style: TextStyle(
                      color: Color(0xFF14264e),
                      fontSize: 13.0,
                      fontFamily: 'MuliFamily',
                    ),
                  ),
                  // const SizedBox(
                  //   height: 4.0,
                  // ),
                  Row(
                    children: [
                      Expanded(
                        child: TextFormField(
                          obscureText: _isObscure,
                          decoration: InputDecoration(
                            border: const UnderlineInputBorder(),
                            labelText: 'Password',
                            focusColor: Colors.black54,
                            suffixIcon: IconButton(
                              icon: Icon(
                                _isObscure
                                    ? Icons.visibility_off
                                    : Icons.visibility,
                              ),
                              onPressed: () => {
                                setState(() {
                                  _isObscure = !_isObscure;
                                })
                              },
                            ),
                          ),
                          style: const TextStyle(
                            fontSize: 14.0,
                          ),
                          // controller: _passwordController,
                        ),
                      ),
                      Visibility(
                        visible: false,
                        child: TextButton(
                          onPressed: () => {},
                          child: const Text(
                            'Generate OTP',
                            style: TextStyle(
                                color: Color(0xff183374),
                                fontSize: 13.0,
                                fontFamily: 'MuliFamily',
                                fontWeight: FontWeight.bold),
                          ),
                        ),
                      ),
                    ],
                  ),
                  const SizedBox(
                    height: 20.0,
                  ),
                  Container(
                    width: double.infinity,
                    margin: const EdgeInsets.symmetric(horizontal: 20),
                    height: 30.0,
                    child: ElevatedButton(
                      onPressed: () => {},
                      child: const Text(
                        'Login',
                        style: TextStyle(fontSize: 12),
                      ),
                      style: ElevatedButton.styleFrom(
                        primary: const Color(0xFF14264e),
                        onPrimary: Colors.white,
                      ),
                    ),
                  ),
                  const SizedBox(
                    height: 16.0,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      TextButton(
                        onPressed: () => {},
                        child: const Text(
                          'Forgot Password',
                          style: TextStyle(
                              color: Color(0xFF14264e),
                              fontSize: 13.0,
                              fontFamily: 'MuliFamily',
                              fontWeight: FontWeight.bold),
                        ),
                      ),
                    ],
                  ),
                  const SizedBox(
                    height: 8.0,
                  ),
                  const Center(
                    child: Text(
                      'Or Login With',
                      style: TextStyle(
                          color: Color(0xFF14264e),
                          fontSize: 11.0,
                          fontFamily: 'MuliFamily',
                          fontWeight: FontWeight.normal),
                    ),
                  ),
                  const SizedBox(
                    height: 16.0,
                  ),
                  Center(
                    child: InkWell(
                      onTap: () => {
                        print('Login with Google')
                      },
                      child: Container(
                        width: 119,
                        height: 30,
                        // child: ElevatedButton.icon(
                        //   onPressed: () => {},
                        //   icon: const Icon(Icons.gps_fixed_rounded),
                        //   label: const Text('Google'),
                        // ),
                        child: Padding(
                          padding: const EdgeInsets.only(right: 10),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Flexible(
                                child: Image.asset(
                                  AssetCatalog.googleLogo,
                                  width: 24,
                                  height: 24,
                                ),
                              ),
                              const Text('Google'),
                              // SizedBox(
                              //   width : 10
                              // )
                            ],
                          ),
                        ),
                        decoration: const BoxDecoration(
                          borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(50),
                            bottomLeft: Radius.circular(50),
                            topRight: Radius.circular(50),
                            bottomRight: Radius.circular(50),
                          ),
                          color: Color(0XFFFFFFFF),
                          boxShadow: [
                            BoxShadow(
                              offset: Offset(0.05, 0.05),
                              color: Colors.grey,
                              blurRadius: 2,
                              spreadRadius: 0.001,
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      const Text(
                        'Not on Entie?',
                        style: TextStyle(
                          fontSize: 11,
                        ),
                      ),
                      // const SizedBox(
                      //   width: 10,
                      // ),
                      TextButton(
                        onPressed: () => {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) =>
                                    const RegistrationScreen()),
                          )
                        },
                        child: const Text(
                          'Register Now',
                          style: TextStyle(
                              fontSize: 12,
                              fontWeight: FontWeight.bold,
                              color: Color(0xFF14264e)),
                        ),
                      ),
                    ],
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
