import 'package:entie_flutter/core/basic_styles/text_styles.dart';
import 'package:entie_flutter/core/local_assets/common_images.dart';
import 'package:entie_flutter/features/login/presentation/screen/login_screen.dart';
import 'package:entie_flutter/features/registration_startup/presentation/screen/startup_screen.dart';
import 'package:flutter/material.dart';

class RegistrationScreen extends StatefulWidget {
  const RegistrationScreen({Key? key}) : super(key: key);

  @override
  State<RegistrationScreen> createState() => _RegistrationScreenState();
}

class _RegistrationScreenState extends State<RegistrationScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        width: double.infinity,
        decoration: NewEntieStyle.bgBoxDecoration,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            const SizedBox(
              height: 43.0,
            ),
            Image.asset(
              AssetCatalog.entieLogo,
              width: 80.0,
              height: 61.0,
            ),
            const SizedBox(
              height: 23.0,
            ),
            /// Yellow strip - CONNECT US
            Container(
              width: double.infinity,
              padding: const EdgeInsets.only(top: 3.0),
              color: const Color(0XFFfeba12),
              child: const Center(
                child: Text(
                  'CONNECT WITH',
                  style: TextStyle(
                      color: Color(0xFFFFFFFF),
                      fontSize: 13.0,
                      fontFamily: 'MuliFamily',
                      fontWeight: FontWeight.bold),
                ),
              ),
            ),

            /// Yellow strip - Entrepe
            Container(
              width: double.infinity,
              padding: const EdgeInsets.only(bottom: 3.0, left: 20, right: 20),
              color: const Color(0XFFfeba12),
              child: const Center(
                child: Text(
                  'Entrepreneurs, Investors, Startups and Manufacturing Business',
                  style: TextStyle(
                    color: Color(0xFF14264e),
                    fontSize: 11,
                    fontFamily: 'MuliFamily',
                    fontWeight: FontWeight.normal,
                  ),
                  textAlign: TextAlign.center,
                ),
              ),
            ),
            const SizedBox(
              height: 26,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                InkWell(
                  onTap: ()=>{
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) =>
                          const StartUpRegistration()),
                    )
                  },
                  child: Container(
                    padding: const EdgeInsets.all(10.0),
                    margin: const EdgeInsets.only(left: 10),
                    width: 142,
                    height: 142,
                    decoration: NewEntieStyle.roleBoxDecoration,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: [
                            SizedBox(
                                width: 60,
                                height: 60,
                                child:
                                    Image.asset(AssetCatalog.startUpRocketIcon),
                            )
                          ],
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: const [
                            Text('I am a\nSTARTUP',
                            style: TextStyle(
                              fontSize: 12.5,
                              fontFamily: 'MuliFamily'
                            ),),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
                const SizedBox(
                  width: 20,
                ),
                InkWell(
                  onTap: ()=>{

                  },
                  child: Container(
                    padding: const EdgeInsets.all(10.0),
                    margin: const EdgeInsets.only(right: 10),
                    width: 142,
                    height: 142,
                    decoration: NewEntieStyle.roleBoxDecoration /*BoxDecoration(
                      color: Color(0Xfff2f2f2),
                      borderRadius: BorderRadius.all(Radius.circular(4)),
                      boxShadow: [
                        BoxShadow(
                          offset: const Offset(0.5, 0.5),
                          color: Colors.grey,
                          blurRadius: 5,
                          spreadRadius: 2,
                        ),
                      ],
                    )*/,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: [
                            SizedBox(
                                width: 60,
                                height: 60,
                                child:
                                    Image.asset(AssetCatalog.investorIcon)),
                          ],
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: const [
                            Text('I am an\nINVESTOR',
                            style: TextStyle(
                              fontSize: 12.5,
                              fontFamily: 'MuliFamily'
                            ),),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
            const SizedBox(
              height: 20,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                InkWell(
                  onTap: ()=>{},
                  child: Container(
                    padding: const EdgeInsets.all(10.0),
                    margin: const EdgeInsets.only(left: 10),
                    width: 142,
                    height: 142,
                    decoration: NewEntieStyle.roleBoxDecoration/*BoxDecoration(
                      color: Color(0Xfff2f2f2),
                      borderRadius: BorderRadius.all(Radius.circular(4)),
                      boxShadow: [
                        BoxShadow(
                          offset: const Offset(0.5, 0.5),
                          color: Colors.grey,
                          blurRadius: 5,
                          spreadRadius: 2,
                        ),
                      ],
                    )*/,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: [
                            SizedBox(
                                width: 60,
                                height: 60,
                                child:
                                    Image.asset(AssetCatalog.cofounderIcon)),
                          ],
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: const [
                            Text('I want to be a\nCO-FOUNDER',
                            style: TextStyle(
                              fontSize: 12.5,
                              fontFamily: 'MuliFamily'
                            ),),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
                const SizedBox(
                  width: 20,
                ),
                InkWell(
                  onTap: ()=>{},
                  child: Container(
                    padding: const EdgeInsets.all(10.0),
                    margin: const EdgeInsets.only(right: 10),
                    width: 142,
                    height: 142,
                    decoration: NewEntieStyle.roleBoxDecoration/*BoxDecoration(
                      color: Color(0Xfff2f2f2),
                      borderRadius: BorderRadius.all(Radius.circular(4)),
                      boxShadow: [
                        BoxShadow(
                          offset: const Offset(0.5, 0.5),
                          color: Colors.grey,
                          blurRadius: 5,
                          spreadRadius: 2,
                        ),
                      ],
                    )*/,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: [
                            SizedBox(
                                width: 60,
                                height: 60,
                                child:
                                    Image.asset(AssetCatalog.manufactureIcon)),
                          ],
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: const [
                            Text('I am a\nMANUFACTURING\nBUSINESS',
                            style: TextStyle(
                              fontSize: 12.5,
                              fontFamily: 'MuliFamily'
                            ),),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
            const SizedBox(
              height: 26,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                const Text(
                  'Already have an account?',
                  style: TextStyle(
                    fontSize: 11,
                  ),
                ),
                // const SizedBox(
                //   width: 10,
                // ),
                TextButton(
                  onPressed: () => {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) =>
                          LoginPageMain()),
                    )
                  },
                  child: const Text(
                    'Log In',
                    style: TextStyle(
                        fontSize: 12, fontWeight: FontWeight.bold,
                        color: Color(0xFF14264e)),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
