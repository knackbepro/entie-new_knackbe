import 'package:entie_flutter/core/basic_styles/text_styles.dart';
import 'package:entie_flutter/core/local_assets/common_images.dart';
import 'package:flutter/material.dart';

class StartUpLooking extends StatefulWidget {
  const StartUpLooking({Key? key}) : super(key: key);

  @override
  _StartUpLookingState createState() => _StartUpLookingState();
}

class _StartUpLookingState extends State<StartUpLooking> {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(8),
      child: Column(
        children: [
          Row(
            children: [
              Expanded(
                flex: 1,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: const [
                    Text(
                      'Hey STARTUP',
                      style: TextStyle(
                        color: Color(0XFF14264e),
                        fontFamily: 'MuliFamily',
                      ),
                    ),
                    Text(
                      'what are the looking for?',
                      style: TextStyle(
                        color: Color(0XFF14264e),
                        fontFamily: 'MuliFamily',
                        fontSize: 12,
                      ),
                    ),
                  ],
                ),
              ),
              Expanded(
                flex: 1,
                child: Column(
                  children: [
                    SizedBox(
                      height: 30,
                      width: double.infinity,
                      child: ElevatedButton(
                        onPressed: () => {},
                        child: Text(
                          'INVESTOR',
                          style: NewEntieStyle.textStyleWhiteXII,
                        ),
                        style: ElevatedButton.styleFrom(
                          primary: const Color(0xFF14264e),
                          onPrimary: Colors.white,
                        ),
                      ),
                    ),
                    const SizedBox(
                      height: 8,
                    ),
                    SizedBox(
                      height: 30,
                      width: double.infinity,
                      child: ElevatedButton(
                        onPressed: () => {},
                        child: Text(
                          'CO-FOUNDER',
                          style: NewEntieStyle.textStyleXII,
                        ),
                        style: ElevatedButton.styleFrom(
                          onPrimary: const Color(0xFF14264e),
                          primary: Color(0Xfff2f2f2),
                        ),
                      ),
                    ),
                    const SizedBox(
                      height: 8,
                    ),
                    SizedBox(
                      height: 30,
                      width: double.infinity,
                      child: ElevatedButton(
                        onPressed: () => {},
                        child: Text(
                          'BUYER for your startup',
                          style: NewEntieStyle.textStyleXII,
                        ),
                        style: ElevatedButton.styleFrom(
                          onPrimary: const Color(0xFF14264e),
                          primary: const Color(0Xfff2f2f2),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
          Container(
            margin: const EdgeInsets.only(top: 16),
            padding: const EdgeInsets.symmetric(vertical: 16, horizontal: 8),
            width: double.infinity,
            decoration: NewEntieStyle.myBoxDecoration,
            /**
             * Investment Ask Box
             * */
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                /**
                 * Investment line*/
                const Text(
                  'Investment Ask',
                  style: TextStyle(
                    color: Color(0XFF707070),
                    fontSize: 12,
                    fontFamily: 'MuliFamily',
                    fontWeight: FontWeight.bold,
                  ),
                ),
                Row(
                  children: [
                    Expanded(
                      child: DropdownButton<String>(
                        elevation: 16,
                        underline: Container(
                          height: 1,
                          color: NewEntieStyle.greyShadeColor,
                        ),
                        onChanged: (String? value) {
                          /// on dropdown Item selected logic will be here.
                        },
                        items: [],
                      ),
                    ),
                    const SizedBox(
                      width: 10,
                    ),
                    Expanded(
                      child: DropdownButton<String>(
                        elevation: 16,
                        underline: Container(
                          height: 1,
                          color: NewEntieStyle.greyShadeColor,
                        ),
                        onChanged: (String? value) {
                          /// on dropdown Item selected logic will be here.
                        },
                        items: [],
                      ),
                    ),
                  ],
                ),
                const SizedBox(
                  height: 20,
                ),
                Row(
                  children: [
                    Expanded(
                      child: Text(
                        'Do you have a Pitch Deck?',
                        style: NewEntieStyle.textStyleXII,
                      ),
                    ),
                    const SizedBox(
                      width: 5,
                    ),
                    Expanded(
                      child: Container(
                        height: 30,
                        margin: const EdgeInsets.symmetric(horizontal: 16),
                        child: ElevatedButton(
                          onPressed: () => {},
                          child: Text(
                            'Upload',
                            style: NewEntieStyle.textStyleWhiteXII,
                          ),
                          style: NewEntieStyle.primaryButtonStyle,
                        ),
                      ),
                    ),
                  ],
                ),
                const SizedBox(
                  height: 16,
                ),
                Text(
                  'Don\'t have a Pitch Deck ? Don\'t worry you can upload it later.',
                  style: NewEntieStyle.textStyleX,
                ),
              ],
            ),
          ),
          const SizedBox(
            height: 20,
          ),
          Row(
            children: [
              Expanded(
                child: SizedBox(
                  height: 30,
                  child: ElevatedButton(
                    child: Text('Back', style: NewEntieStyle.textStyleWhiteXII),
                    onPressed: () => {},
                    style: NewEntieStyle.primaryButtonStyle,
                  ),
                ),
              ),
              const SizedBox(width: 10),
              Expanded(
                child: SizedBox(
                  height: 30,
                  child: ElevatedButton(
                    child: Text('Next', style: NewEntieStyle.textStyleWhiteXII),
                    onPressed: () => {},
                    style: NewEntieStyle.primaryButtonStyle,
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
