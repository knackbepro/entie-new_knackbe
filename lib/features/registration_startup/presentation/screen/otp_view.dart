import 'package:entie_flutter/core/basic_styles/text_styles.dart';
import 'package:entie_flutter/core/local_assets/common_images.dart';
import 'package:flutter/material.dart';
import 'package:otp_text_field/otp_field.dart';
import 'package:otp_text_field/style.dart';

class OtpViewDialog extends StatelessWidget {
  const OtpViewDialog({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        margin: const EdgeInsets.only(top: 200),
        padding: const EdgeInsets.all(8),
        width: double.infinity,
        decoration: const BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(4),
            topRight: Radius.circular(4),
          ),
        ),
        child: Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                GestureDetector(
                  onTap: ()=>{
                    Navigator.pop(context)
                  },
                  child: Image.asset(
                    AssetCatalog.closeIcon,
                    width: 17,
                    height: 17,
                  ),
                ),
              ],
            ),
            Text(
              'Verify your mobile',
              style: NewEntieStyle.textStyleXXX,
            ),
            const SizedBox(
              height: 10,
            ),
            Text(
              'A one time password has been sent\nvia SMS to verify your mobile number',
              style: NewEntieStyle.textStyleXIII,
            ),
            const SizedBox(
              height: 20,
            ),
            Flexible(
              /// OTP View
              child: OTPTextField(
                length: 4,
                width: double.infinity,
                fieldWidth: 80,
                style: const TextStyle(fontSize: 17),
                textFieldAlignment: MainAxisAlignment.spaceAround,
                fieldStyle: FieldStyle.underline,
                onCompleted: (pin) {
                  print("Completed: " + pin);
                },
              ),
            ),
            const SizedBox(
              height: 18.5,
            ),
            Container(
              width: double.infinity,
              height: 30,
              margin: const EdgeInsets.symmetric(horizontal: 30),
              child: ElevatedButton(
                onPressed: () => {},
                child: Text(
                  'Submit',
                  style: NewEntieStyle.textStyleWhiteXII,
                ),
                style: ElevatedButton.styleFrom(
                  primary: const Color(0xFF14264e),
                  onPrimary: Colors.white,
                ),
              ),
            ),
            const SizedBox(
              height: 5,
            ),
            TextButton(
              onPressed: () => {},
              child: Text(
                'Resend OTP',
                style: NewEntieStyle.textStyleXII,
              ),
            )
          ],
        ),
      ),
    );
  }
}
