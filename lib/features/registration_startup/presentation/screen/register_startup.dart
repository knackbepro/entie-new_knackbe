import 'package:entie_flutter/core/basic_styles/text_styles.dart';
import 'package:flutter/material.dart';

class StartUpRegisterHere extends StatelessWidget {
  const StartUpRegisterHere({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        width: double.infinity,
        margin: const EdgeInsets.only(top: 30),
        decoration: NewEntieStyle.bgBoxDecoration,
        child: Column(
          children: [
            const SizedBox(
              height: 30,
            ),
            Text(
              'What we have for you',
              style: NewEntieStyle.textStyleXX,
            ),
            const SizedBox(
              height: 30,
            ),
            Container(
              decoration: BoxDecoration(
                  borderRadius: const BorderRadius.all(Radius.circular(8)),
                  color: NewEntieStyle.primaryYellowColor),
              width: double.infinity,
              margin: const EdgeInsets.all(10),
              padding: const EdgeInsets.all(12),
              child: Center(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  // crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Text(
                      '71',
                      style: TextStyle(
                        fontSize: 40,
                        fontFamily: 'MuliFamily',
                        color: NewEntieStyle.primaryColor,
                      ),
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: const [
                        Text(
                          'Investors',
                          style: TextStyle(
                              fontSize: 16,
                              fontFamily: 'MuliFamily',
                              fontWeight: FontWeight.bold),
                        ),
                        Text('looking for startups to invest'),
                      ],
                    )
                  ],
                ),
              ),
            ),
            const SizedBox(
              height: 30,
            ),
            const Text(
              'Want to connect?',
              style: TextStyle(
                  color: Color(0xFF14264e),
                  fontSize: 20,
                  fontFamily: 'MuliFamily'),
            ),
            const SizedBox(
              height: 16,
            ),
            Container(
              width: double.infinity,
              height: 30,
              margin: const EdgeInsets.symmetric(horizontal: 30),
              child: ElevatedButton(
                onPressed: () => {},
                child: Text(
                  'Register',
                  style: NewEntieStyle.textStyleWhiteXII,
                ),
                style: ElevatedButton.styleFrom(
                  primary: const Color(0xFF14264e),
                  onPrimary: Colors.white,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
