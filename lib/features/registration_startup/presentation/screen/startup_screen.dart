import 'package:entie_flutter/core/basic_styles/text_styles.dart';
import 'package:entie_flutter/core/local_assets/common_images.dart';
import 'package:entie_flutter/features/login/presentation/screen/login_screen.dart';
import 'package:entie_flutter/features/registration/presentation/screen/startup_page1.dart';
import 'package:entie_flutter/features/registration_startup/presentation/screen/otp_view.dart';
import 'package:entie_flutter/features/registration_startup/presentation/screen/register_startup.dart';
import 'package:flutter/material.dart';

class StartUpRegistration extends StatelessWidget {
  const StartUpRegistration({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
          width: double.infinity,
          margin: const EdgeInsets.only(top: 30),
          decoration: NewEntieStyle.bgBoxDecoration,
          child: StartUpLooking()),
          // child: StartUpRegisterHere()),
          // child: OtpViewDialog()),
    );
  }
}
