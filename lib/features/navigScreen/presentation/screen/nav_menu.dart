import 'package:entie_flutter/core/basic_styles/text_styles.dart';
import 'package:entie_flutter/core/local_assets/common_images.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class NavMenuScreen extends StatelessWidget {
  NavMenuScreen({Key? key}) : super(key: key);

  var itemPadding =
      const EdgeInsets.only(left: 21, top: 10, bottom: 10, right: 21);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: NewEntieStyle.greyColor,
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            /// User Intro Name
            Container(
              color: NewEntieStyle.primaryYellowColor,
              padding: const EdgeInsets.only(top: 25, bottom: 25),
              child: Row(
                children: [
                  const SizedBox(
                    width: 21,
                  ),
                  CircleAvatar(
                    backgroundImage: AssetImage(AssetCatalog.entieLogo),
                    radius: 30,
                  ),
                  const SizedBox(
                    width: 11,
                  ),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'Meticulous Business Solutions',
                        style: NewEntieStyle.textStyleWhiteXIV,
                      ),
                      Text(
                        'Sumit Patil, CTO',
                        style: NewEntieStyle.textStyleWhiteX,
                      ),
                    ],
                  ),
                ],
              ),
            ),

            /// Editable options
            const SizedBox(
              height: 10,
            ),

            /// My Profile
            Container(
              padding: itemPadding,
              child: Text(
                'My Profile',
                style: NewEntieStyle.textStyleXII,
              ),
            ),

            /// My Interest
            Container(
              padding: itemPadding,
              child: Text(
                'My Intrests',
                style: NewEntieStyle.textStyleXII,
              ),
            ),

            /// KNowledge Center
            Container(
              padding: itemPadding,
              child: Text(
                'Knowledge Center',
                style: NewEntieStyle.textStyleXII,
              ),
            ),

            /// My Followers
            Container(
              padding: itemPadding,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    'My Followers',
                    style: NewEntieStyle.textStyleXII,
                  ),
                  Text(
                    '1',
                    style: NewEntieStyle.textStyleXII,
                  ),
                ],
              ),
            ),

            /// Following
            Container(
              padding: itemPadding,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    'Following',
                    style: NewEntieStyle.textStyleXII,
                  ),
                  Text(
                    '0',
                    style: NewEntieStyle.textStyleXII,
                  ),
                ],
              ),
            ),

            /// Profile View Count
            Container(
              padding: itemPadding,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    'Profile View Count',
                    style: NewEntieStyle.textStyleXII,
                  ),
                  Text(
                    '10 Views',
                    style: NewEntieStyle.textStyleXII,
                  ),
                ],
              ),
            ),
            Divider(
              color: NewEntieStyle.greyShade1Color,
              height: 0.5,
            ),
            const SizedBox(
              height: 12,
            ),

            /// Entie Subscriptions
            Padding(
              padding:
                  const EdgeInsets.only(left: 21, top: 5, bottom: 5, right: 21),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text(
                    'My Subscription',
                    style: NewEntieStyle.textStyleGreyXII,
                  ),
                  Text(
                    'Expires on 20/04/2022',
                    style: NewEntieStyle.textStyleGreyX,
                  ),
                ],
              ),
            ),
            const SizedBox(
              height: 10,
            ),
            Padding(
              padding:
                  const EdgeInsets.only(left: 21, top: 5, bottom: 5, right: 21),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text(
                    'Membership',
                    style: NewEntieStyle.textStyleGreyXII,
                  ),
                  Text(
                    '| Go Premium',
                    style: TextStyle(
                        color: NewEntieStyle.primaryColor,
                        fontSize: 10,
                        fontFamily: 'MuliFamily',
                        fontWeight: FontWeight.bold),
                  ),
                ],
              ),
            ),
            const SizedBox(
              height: 10,
            ),
            Padding(
              padding:
                  const EdgeInsets.only(left: 21, top: 5, bottom: 5, right: 21),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text(
                    'Profile Verified',
                    style: NewEntieStyle.textStyleGreyXII,
                  ),
                  Image.asset(
                    AssetCatalog.verifiedIcon,
                    width: 16,
                    height: 16,
                  )
                ],
              ),
            ),
            const SizedBox(
              height: 10,
            ),
            Padding(
              padding:
                  const EdgeInsets.only(left: 21, top: 5, bottom: 5, right: 21),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text(
                    'Ratings',
                    style: NewEntieStyle.textStyleGreyXII,
                  ),
                  Text(
                    '| Get Ratings',
                    style: TextStyle(
                        color: NewEntieStyle.primaryColor,
                        fontSize: 10,
                        fontFamily: 'MuliFamily',
                        fontWeight: FontWeight.bold),
                  ),
                ],
              ),
            ),
            const SizedBox(
              height: 10,
            ),
            Divider(
              color: NewEntieStyle.greyShade1Color,
              height: 0.5,
            ),
            const SizedBox(
              height: 5,
            ),

            /// Static pages
            Container(
              padding: itemPadding,
              child: Text(
                'Help & Support',
                style: TextStyle(
                    color: NewEntieStyle.primaryColor,
                    fontSize: 12,
                    fontFamily: 'MuliFamily',
                    fontWeight: FontWeight.bold),
              ),
            ),
            Container(
              padding: itemPadding,
              child: Text(
                'Terms & Conditions',
                style: TextStyle(
                    color: NewEntieStyle.primaryColor,
                    fontSize: 12,
                    fontFamily: 'MuliFamily',
                    fontWeight: FontWeight.bold),
              ),
            ),
            Container(
              padding: itemPadding,
              child: Text(
                'About Entie',
                style: TextStyle(
                    color: NewEntieStyle.primaryColor,
                    fontSize: 12,
                    fontFamily: 'MuliFamily',
                    fontWeight: FontWeight.bold),
              ),
            ),
            const SizedBox(
              height: 5,
            ),
            Divider(
              color: NewEntieStyle.greyShadeColor,
              height: 0.5,
            ),
            Container(
              padding: itemPadding,
              child: Text(
                'Logout',
                style: TextStyle(
                    color: NewEntieStyle.primaryColor,
                    fontSize: 12,
                    fontFamily: 'MuliFamily',
                    fontWeight: FontWeight.bold),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
