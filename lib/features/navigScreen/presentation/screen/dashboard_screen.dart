import 'package:entie_flutter/core/basic_styles/text_styles.dart';
import 'package:entie_flutter/features/navigScreen/presentation/widget/basic_matches.dart';
import 'package:entie_flutter/features/navigScreen/presentation/widget/curated_matches.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';


class MainDashboard extends StatelessWidget {
  const MainDashboard({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    TabController? _tabController;
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: DefaultTabController(
        length: 2,
        child: Scaffold(
          body: Column(
            children: [
              /// 'Results For' app bar
              Container(
                color: Colors.white,
                width: MediaQuery.of(context).size.width,
                height: 36.0,
                child: Stack(
                  children: [
                    Positioned(
                      top: 10,
                      bottom: 0,
                      left: 10,
                      child: Text(
                        'Results For',
                        style: NewEntieStyle.textStyleXII,
                      ),
                    ),
                    Positioned(
                      top: 7,
                      left: 80,
                      child: SizedBox(
                        width: 70.0,
                        height: 20.0,
                        child: ElevatedButton(
                          onPressed: () => {},
                          child: Text(
                            'Investor',
                            style: NewEntieStyle.textStyleWhiteX,
                          ),
                          style: ElevatedButton.styleFrom(
                            primary: const Color(0xFF14264e),
                            onPrimary: Colors.white,
                          ),
                        ),
                      ),
                    ),
                    Positioned(
                      top: 7,
                      left: 155,
                      child: SizedBox(
                        width: 85.0,
                        height: 20.0,
                        child: ElevatedButton(
                          onPressed: () => {},
                          child: Text(
                            'Co-founder',
                            style: NewEntieStyle.textStyleWhiteX,
                          ),
                          style: ElevatedButton.styleFrom(
                            primary: const Color(0xFF14264e),
                            onPrimary: Colors.white,
                          ),
                        ),
                      ),
                    ),
                    Positioned(
                      top: 7,
                      left: 245,
                      child: SizedBox(
                        width: 60.0,
                        height: 20.0,
                        child: ElevatedButton(
                          onPressed: () => {},
                          child: Text(
                            'Buyer',
                            style: NewEntieStyle.textStyleWhiteX,
                          ),
                          style: ElevatedButton.styleFrom(
                            primary: const Color(0xFF14264e),
                            onPrimary: Colors.white,
                          ),
                        ),
                      ),
                    ),
                    Positioned(
                      top: 7,
                      right: 5,
                      child: Icon(
                        Icons.filter_alt_outlined,
                        color: NewEntieStyle.primaryColor,
                      ),
                    )
                  ],
                ),
              ),
              /// tab views
              Container(
                color: Colors.grey.shade100,
                height: 30.0,
                child: const TabBar(
                  labelColor: Colors.grey,
                  unselectedLabelColor: Color(0XFF880088),
                  tabs: [
                    Tab(text: 'Curated Matches'),
                    Tab(text: 'Matches'),
                  ],
                ),
              ),

              /// Tab switching screens
              Expanded(
                child: TabBarView(
                  controller: _tabController,
                  children: [
                    CuratedMatches(),
                    BasicMatches(),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
