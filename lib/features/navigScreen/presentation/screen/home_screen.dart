import 'package:entie_flutter/core/basic_styles/text_styles.dart';
import 'package:entie_flutter/core/local_assets/common_images.dart';
import 'package:entie_flutter/features/navigScreen/presentation/screen/dashboard_screen.dart';
import 'package:entie_flutter/features/navigScreen/presentation/screen/nav_menu.dart';
import 'package:flutter/material.dart';

class HomeBottomNav extends StatefulWidget {
  const HomeBottomNav({Key? key}) : super(key: key);

  @override
  State<HomeBottomNav> createState() => _HomeBottomNavState();
}

class _HomeBottomNavState extends State<HomeBottomNav> {
  int currentIndex = 0;
  static const double navIconSize = 20;

  /// Screens for bottom navigation items
  final screens = [
    const MainDashboard(),
    // const Center(
    //   child: Text(
    //     'Dashboard',
    //     style: TextStyle(fontSize: 20),
    //   ),
    // ),
    const Center(
      child: Text(
        'Message',
        style: TextStyle(fontSize: 20),
      ),
    ),
    const Center(
      child: Text(
        'Posts',
        style: TextStyle(fontSize: 20),
      ),
    ),
    const Center(
      child: Text(
        'Notifications',
        style: TextStyle(fontSize: 20),
      ),
    ),
    NavMenuScreen(),
    // const SettingsScreen(),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          const SizedBox(
            height: 35.5,
          ),
          Container(
            color: NewEntieStyle.primaryYellowColor,
            width: double.infinity,
            height: 40.0,

            /// Custom App Bar
            child: Stack(
              children: [
                Positioned(
                  left: 0,
                  top: 0,
                  bottom: 0,
                  child: Padding(
                    padding: const EdgeInsets.all(5.0),
                    child: Image.asset(
                      AssetCatalog.entieLogo,
                    ),
                  ),
                ),
                Positioned(
                  child: Center(
                    child: Text('Startup'),
                  ),
                ),
                // Positioned(
                //   right: 50,
                //   child: Padding(
                //     padding: const EdgeInsets.all(5.0),
                //     child: Icon(
                //       Icons.search,
                //       size: 30,
                //       color: NewEntieStyle.primaryColor,
                //     ),
                //   ),
                // ),
                Positioned(
                  right: 0,
                  child: Padding(
                    padding: const EdgeInsets.all(5.0),
                    child: Icon(
                      Icons.search,
                      size: 30,
                      color: NewEntieStyle.primaryColor,
                    ),
                  ),
                ),
              ],
            ),
          ),
          Expanded(
            child: IndexedStack(
              index: currentIndex,
              children: screens,
            ),
          ),
        ],
      ),
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: currentIndex,
        type: BottomNavigationBarType.fixed,
        selectedItemColor: NewEntieStyle.primaryYellowColor,
        backgroundColor: NewEntieStyle.primaryColor,
        iconSize: 24,
        selectedFontSize: 14,
        showUnselectedLabels: true,
        showSelectedLabels: true,
        unselectedItemColor: Colors.white,
        onTap: (index) => setState(() => currentIndex = index),
        items: const [
          BottomNavigationBarItem(
            icon: Icon(
              Icons.home,
              size: navIconSize,
            ),
            label: 'Dashboard',
          ),
          BottomNavigationBarItem(
            icon: Icon(
              Icons.chat,
              size: navIconSize,
            ),
            label: 'Message',
          ),
          BottomNavigationBarItem(
            icon: Icon(
              Icons.post_add_outlined,
              size: navIconSize,
            ),
            label: 'Posts',
          ),
          BottomNavigationBarItem(
            icon: Icon(
              Icons.notifications_active,
              size: navIconSize,
            ),
            label: 'Notifications',
          ),
          BottomNavigationBarItem(
            icon: Icon(
              Icons.blur_circular_rounded,
              size: navIconSize,
            ),
            label: 'Menu',
          ),
        ],
      ),
    );
  }
}
